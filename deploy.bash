#!/bin/bash

# Deployment script for customized message of the day (`motd`) files.

# Simply copies the customized files into `/etc/update-motd.d` to be run on login.

echo "Cloning from GitLab..."
git clone git@gitlab.com:alexlab-cloud/infrastructure/homelab/motd.git alexlab-motd

echo "Removing existing /etc/update-motd.d files..."
sudo rm -r /etc/update-motd.d/*

echo "Copying files to /etc/update-motd.d"
sudo cp alexlab-motd/etc/update-motd.d/* /etc/update-motd.d/

sudo rm -rf alexlab-motd
