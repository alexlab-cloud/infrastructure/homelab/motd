#!/bin/bash
# ---------
# This script will update `PrintMotd` to `yes` in the /etc/ssh/sshd_config file and remove unecessary scripts.
#
# https://superuser.com/a/759505

sed -re 's/^(PrintMotd)([[:space:]]+)no/\1\2yes/' -i."$(date -I)" /etc/ssh/sshd_config || true

