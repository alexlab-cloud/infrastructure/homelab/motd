#!/bin/bash

# Extension script containing shell commands that are run once the container has been started.

true # Allows the below ↓ shellcheck disable to only affect the line below it
# https://stackoverflow.com/a/62036949/6683107

# Allow Git to be used in the development container
# shellcheck disable=SC2154
git config --global --add safe.directory "${CONTAINER_WORKSPACE_FOLDER}"
