#!/bin/bash

# Extension script containing shell commands that are run once the container has been created.

true # Allows the below ↓ shellcheck disable to only affect the line below it
# https://stackoverflow.com/a/62036949/6683107

# Provide permissions to the session to create and delete files
# shellcheck disable=SC2154
sudo chown -R vscode "${CONTAINER_WORKSPACE_FOLDER}"
sudo chmod -R 775 .git
