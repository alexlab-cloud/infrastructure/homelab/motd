# Message of the Day: AlexLab Edition

Custom [message of the day (`motd`)](https://manpages.ubuntu.com/manpages/~/en/man5/motd.5.html) scripting for homelab customization.

## Installation

The installation/deployment script simply copies the customized files to `/etc/update-motd.d` to be run on login.

```
wget https://gitlab.com/alexlab-cloud/infrastructure/homelab/motd/-/raw/main/deploy.bash
chmod +x deploy.bash
./deploy.bash
```

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>
